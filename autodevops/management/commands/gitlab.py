import subprocess

from git import Repo
import gitlab
from django.core.management.base import BaseCommand, CommandError
from environ import Env
import os
from django.conf import settings
from urllib.parse import urlencode, quote_plus

env = Env()
env_file = os.path.join(settings.BASE_DIR, 'www/.env')
print(env_file)
if os.path.isfile(env_file):
    env.read_env(env_file=env_file)


def get_username(hostname):
    process = subprocess.Popen(['ssh', '-T', 'git@' + hostname],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout.decode('UTF-8').split('@', 1)[-1][:-2]

def get_hostname():
    import platform
    return platform.node()

class Command(BaseCommand):
    help = 'Sets up GitLab for local development.'

    def handle(self, *args, **options):
        self.stdout.write('Running GitLab Command')


        repo = Repo('.')
        split_url = repo.remotes[0].url.split('@', 1)[-1].split(':', 1)
        gitlab_hostname = split_url[0]
        origin = 'https://' + split_url[0]
        url = 'https://' + '/'.join(split_url)
        username = get_username(gitlab_hostname)
        project_name = split_url[1].rsplit('.', 1)[0]
        project_id = quote_plus(project_name)
        token = env('GITLAB_TOKEN', default='')
        gl = None
        try:
            gl = gitlab.Gitlab(url=origin, private_token=token)
            gl.auth()
        except gitlab.exceptions.GitlabAuthenticationError:
            token = ''

        if not token:
            print(get_hostname())
            print(project_name)
            print(username)
            print(origin)
            print(url)
            self.stdout.write('Please create a new Personal Access Token:')
            self.stdout.write('- ' + origin + '/profile/personal_access_tokens')
            self.stdout.write('- Name: ' + project_name + ' on ' + get_hostname())
            self.stdout.write('- Expires At: ')
            self.stdout.write('- API')
            self.stdout.write('Append this to the file www/.env: GITLAB_TOKEN=')
            exit(1)

        project = gl.projects.get(project_id)
        for key in ['UNLEASH_API_URL', 'UNLEASH_API_IID', 'SENTRY_DSN']:
            updates = {
                'value': env(key)
            }
            if updates:
                try:
                    variable = project.variables.get('K8S_SECRET_' + key)
                    for k, v in updates.items():
                        setattr(variable, k, v)
                    variable.save()
                except gitlab.exceptions.GitlabGetError:
                    updates['key'] = 'K8S_SECRET_' + key
                    project.variables.create(updates)

        print(url.rsplit('.', 1)[0] + '/-/settings/ci_cd#js-variables')